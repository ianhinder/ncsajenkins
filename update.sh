#!/bin/bash

set -e

# Stop the container
if docker ps | grep etslave >/dev/null 2>/dev/null; then
    docker kill etslave
fi

# Remove the container
if docker ps -a | grep etslave >/dev/null 2>&1; then
    docker rm etslave
fi

# Update the image
docker pull ianhinder/et-jenkins-slave:ubuntu-16.04

# Restart the container with the new image
./run.sh

echo "Jenkins build slave successfully updated"
